package sample.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Gataullin Kamil
 * 15.05.2016 22:12
 */
public class MessageInfo {

    private final StringProperty addedTime;
    private final StringProperty text;

    public MessageInfo() {
        this(null, null);
    }

    public MessageInfo(String addedTime, String text) {
        this.addedTime = new SimpleStringProperty(addedTime);
        this.text = new SimpleStringProperty(text);
    }

    public String getAddedTime() {
        return addedTime.get();
    }

    public StringProperty addedTimeProperty() {
        return addedTime;
    }

    public void setAddedTime(String addedTime) {
        this.addedTime.set(addedTime);
    }

    public String getText() {
        return text.get();
    }

    public StringProperty textProperty() {
        return text;
    }

    public void setText(String text) {
        this.text.set(text);
    }
}
