package sample.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import sample.model.MessageInfo;

import java.text.SimpleDateFormat;
import java.util.Date;


public class Controller {

    @FXML
    private TextField myNameField;
    @FXML
    private Label myNameLabel;

    @FXML
    private TableView<MessageInfo> tableView;
    @FXML
    private TableColumn<MessageInfo, String> firstColumnDate;
    @FXML
    private TableColumn<MessageInfo, String> secondColumnText;

    private ObservableList<MessageInfo> messages = FXCollections.observableArrayList();
    @FXML
    private TextField messageField;

    @FXML
    private void initialize() {
        blur();
        // Инициализация таблицы адресатов с двумя столбцами.
        firstColumnDate.setCellValueFactory(cellData -> cellData.getValue().addedTimeProperty());
        secondColumnText.setCellValueFactory(cellData -> cellData.getValue().textProperty());

        tableView.setItems(messages);
    }

    @FXML
    private void hello() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Заголовок");
        alert.setHeaderText("Привет!");
        alert.setContentText("Приветствую " + myNameField.getText());
        alert.showAndWait();
    }

    @FXML
    private void blur() {
        myNameLabel.setText(myNameField.getText());
    }

    @FXML
    private void addedText() {
        String message = messageField.getText();
        if (message != null && !message.isEmpty()) {
            SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss");

            messages.add(new MessageInfo(format.format(new Date()), message));
            messageField.setText("");
        }
    }
}
